<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'devoneti_ott' );

/** MySQL database username */
define( 'DB_USER', 'devoneti_usrott' );

/** MySQL database password */
define( 'DB_PASSWORD', '3V0;sW,?&_Ey' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '0*YN%+m.eF8$Ak3N58mr/c*WVlAU4([S(KE)<vAMSTUTC+IpKe@Sj2UO^Nu=j76 ' );
define( 'SECURE_AUTH_KEY',  'wv|xsX&d#9fx`~40R6N`G#qs8s6*gg~?4ZnYG/m(3]#P9D$muz1<A6wP9|D2*rp=' );
define( 'LOGGED_IN_KEY',    '2tQ|zIjY*eEXMjSs~#`;`%um{*~g%{iqm,n%sp;|hK=>ZML<l{(<(WTY44:,3~5m' );
define( 'NONCE_KEY',        'V%#2r8yg[tfE@,c&lPKbm*,bcGZl$0uAO)Z-.+c`e8whV)9g%jI4BYb,(x:J!%Mz' );
define( 'AUTH_SALT',        'iy,5>j|AA!xw@f`{a4u{v(H%%&%Nz.-+7 VkzVWD@j39[(wT4+<C}2L@ac]bbK<?' );
define( 'SECURE_AUTH_SALT', 'lA%d7/dhM:;Z@Wu`aS;[@^U%<{.t4[&=DrwoG9[(mi3sdf^RmbHD}C*ZALW(NS(W' );
define( 'LOGGED_IN_SALT',   '4B=v9(| .,F&!qob/LRx%>vp}mh5{f2l8Z|% GuU:4b+J7u9At_P5StR2EmJu;@0' );
define( 'NONCE_SALT',       'xyu;=F-a~hC]LxaA}B/J${ToMFPXz>Y7F_D@3Itsq3(CuWjUI$=u=UM7R,lkxJ h' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
