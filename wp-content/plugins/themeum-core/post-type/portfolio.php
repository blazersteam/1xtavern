<?php
function corlate_post_type_portfolio(){

    $labels = array(
            'name'                  => _x( 'Portfolios', 'Portfolios', 'themeum-core' ),
            'singular_name'         => _x( 'Portfolio', 'Portfolio', 'themeum-core' ),
            'menu_name'             => __( 'Portfolios', 'themeum-core' ),
            'parent_item_colon'     => __( 'Parent Portfolio:', 'themeum-core' ),
            'all_items'             => __( 'All Portfolio', 'themeum-core' ),
            'view_item'             => __( 'View Portfolio', 'themeum-core' ),
            'add_new_item'          => __( 'Add New Portfolio', 'themeum-core' ),
            'add_new'               => __( 'New Portfolio', 'themeum-core' ),
            'edit_item'             => __( 'Edit Portfolio', 'themeum-core' ),
            'update_item'           => __( 'Update Portfolio', 'themeum-core' ),
            'search_items'          => __( 'Search Portfolio', 'themeum-core' ),
            'not_found'             => __( 'No article found', 'themeum-core' ),
            'not_found_in_trash'    => __( 'No article found in Trash', 'themeum-core' )
        );

    $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'hierarchical'          => false,
            'menu_icon'             => 'dashicons-format-gallery',
            'menu_position'         => null,
            'supports'              => array( 'title','editor','thumbnail','comments')
        );

    register_post_type('portfolio',$args);
}
add_action('init','corlate_post_type_portfolio');


/*--------------------------------------------------------------
 *          View Message When Updated portfolio
 *-------------------------------------------------------------*/
function corlate_update_message_portfolio(){
    global $post, $post_ID;

    $message['portfolio'] = array(
        0   => '',
        1   => sprintf( __('Portfolio updated. <a href="%s">View Portfolio</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
        2   => __('Custom field updated.', 'themeum-core' ),
        3   => __('Custom field deleted.', 'themeum-core' ),
        4   => __('Portfolio updated.', 'themeum-core' ),
        5   => isset($_GET['revision']) ? sprintf( __('Portfolio restored to revision from %s', 'themeum-core' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
        6   => sprintf( __('Portfolio published. <a href="%s">View Portfolio</a>', 'themeum-core' ), esc_url( get_permalink($post_ID) ) ),
        7   => __('Portfolio saved.', 'themeum-core' ),
        8   => sprintf( __('Portfolio submitted. <a target="_blank" href="%s">Preview Portfolio</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        9   => sprintf( __('Portfolio scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Portfolio</a>', 'themeum-core' ), date_i18n( __( 'M j, Y @ G:i','themeum-core'), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
        10  => sprintf( __('Portfolio draft updated. <a target="_blank" href="%s">Preview Portfolio</a>', 'themeum-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        );

return $message;
}
add_filter( 'post_updated_messages', 'corlate_update_message_portfolio' );



/*--------------------------------------------------------------
 *          Register Custom Taxonomies
 *-------------------------------------------------------------*/
function corlate_create_portfolio_taxonomy(){
    $labels = array(    'name'              => _x( 'Categories', 'taxonomy general name','themeum-core'),
                        'singular_name'     => _x( 'Category', 'taxonomy singular name','themeum-core' ),
                        'search_items'      => __( 'Search Category','themeum-core'),
                        'all_items'         => __( 'All Category','themeum-core'),
                        'parent_item'       => __( 'Parent Category','themeum-core'),
                        'parent_item_colon' => __( 'Parent Category:','themeum-core'),
                        'edit_item'         => __( 'Edit Category','themeum-core'),
                        'update_item'       => __( 'Update Category','themeum-core'),
                        'add_new_item'      => __( 'Add New Category','themeum-core'),
                        'new_item_name'     => __( 'New Category Name','themeum-core'),
                        'menu_name'         => __( 'Category','themeum-core')
        );
    $args = array(  'hierarchical'      => true,
                    'labels'            => $labels,
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    'query_var'         => true,
        );
    register_taxonomy('portfolio-cat',array( 'portfolio' ),$args);
}

add_action('init','corlate_create_portfolio_taxonomy');


/**
 * Register Portfolio Tag Taxonomies
 *
 * @return void
 */


function corlate_register_portfolio_tag_taxonomy(){
    $labels = array(
        'name'                  => _x( 'Portfolio Tags', 'taxonomy general name', 'themeum-core' ),
        'singular_name'         => _x( 'Portfolio Tag', 'taxonomy singular name', 'themeum-core' ),
        'search_items'          => __( 'Search Portfolio Tag', 'themeum-core' ),
        'all_items'             => __( 'All Portfolio Tag', 'themeum-core' ),
        'parent_item'           => __( 'Portfolio Parent Tag', 'themeum-core' ),
        'parent_item_colon'     => __( 'Portfolio Parent Tag:', 'themeum-core' ),
        'edit_item'             => __( 'Edit Portfolio Tag', 'themeum-core' ),
        'update_item'           => __( 'Update Portfolio Tag', 'themeum-core' ),
        'add_new_item'          => __( 'Add New Portfolio Tag', 'themeum-core' ),
        'new_item_name'         => __( 'New Portfolio Tag Name', 'themeum-core' ),
        'menu_name'             => __( 'Portfolio Tag', 'themeum-core' )
    );

    $args = array(
        'hierarchical'          => false,
        'labels'                => $labels,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'query_var'             => true
    );
    register_taxonomy( 'portfolio-tag', array( 'portfolio' ), $args );
}
add_action('init','corlate_register_portfolio_tag_taxonomy');