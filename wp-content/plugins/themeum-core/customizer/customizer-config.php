<?php

$panel_to_section = array(
	'id'           => 'corlate_panel_options',
	'title'        => esc_html( 'Corlate Options', 'themeum-core' ),
	'description'  => esc_html__( 'Corlate Theme Options', 'themeum-core' ),
	'priority'     => 10,
	'sections'     => array(

		array(
			'id'              => 'header_setting',
			'title'           => esc_html__( 'Header Settings', 'themeum-core' ),
			'description'     => esc_html__( 'Header Settings', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'enable_topbar',
					'label'    => esc_html__( 'Header Top Enable', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'header_phone',
					'label'    => esc_html__( 'Header Top Email', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '+0123 456 70 90',
				),
				array(
					'settings' => 'topbar_social',
					'label'    => esc_html__( 'Header Top Social', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'head_style',
					'label'    => esc_html__( 'Select Header Style', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'transparent',
					'choices'  => array(
						'transparent' => esc_html( 'Header Transparent', 'themeum-core' ),
						'solid' => esc_html( 'Header Solid', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'topbar_bg',
					'label'    => esc_html__( 'Topbar background Color', 'themeum-core' ),
					'type'     => 'rgba',
					'priority' => 10,
					'default'  => '#191919',
				),
				array(
					'settings' => 'header_color',
					'label'    => esc_html__( 'Header background Color', 'themeum-core' ),
					'type'     => 'rgba',
					'priority' => 10,
					'default'  => '#151515',
					'dependency' => array(
						'id' => 'head_style',
						'comp' => '!=',
						'value' => 'transparent',
					),
				),
				array(
					'settings' => 'header_padding_top',
					'label'    => esc_html__( 'Header Top Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 0,
				),
				array(
					'settings' => 'header_padding_bottom',
					'label'    => esc_html__( 'Header Bottom Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 0,
				),
				array(
					'settings' => 'content_padding_top',
					'label'    => esc_html__( 'Content Top Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 75,
				),
				array(
					'settings' => 'header_fixed',
					'label'    => esc_html__( 'Sticky Header', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'sticky_header_color',
					'label'    => esc_html__( 'Sticky background Color', 'themeum-core' ),
					'type'     => 'rgba',
					'priority' => 10,
					'default'  => '#222538',
				),
		
				
			)//fields
		),//header_setting


		array(
			'id'              => 'logo_setting',
			'title'           => esc_html__( 'All Logo', 'themeum-core' ),
			'description'     => esc_html__( 'All Logo', 'themeum-core' ),
			'priority'        => 10,
			// 'active_callback' => 'is_front_page',
			'fields'         => array(
				array(
					'settings' => 'logo_style',
					'label'    => esc_html__( 'Select Header Style', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'logoimg',
					'choices'  => array(
						'logoimg' => esc_html( 'Logo image', 'themeum-core' ),
						'logotext' => esc_html( 'Logo text', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'logo',
					'label'    => esc_html__( 'Upload Logo', 'themeum-core' ),
					'type'     => 'upload',
					'priority' => 10,
					'default' => get_template_directory_uri().'/images/logo.png',
				),
				array(
					'settings' => 'logo_width',
					'label'    => esc_html__( 'Logo Width', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 97,
				),
				array(
					'settings' => 'logo_height',
					'label'    => esc_html__( 'Logo Height', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
				),
				array(
					'settings' => 'logo_text',
					'label'    => esc_html__( 'Custom Logo Text', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => 'themeum-core',
				),
				array(
					'settings' => 'logo-404',
					'label'    => esc_html__( 'Coming Soon Logo', 'themeum-core' ),
					'type'     => 'upload',
					'priority' => 10,
					'default'  => get_template_directory_uri().'/images/logo-404.png',
				),		
			)//fields
		),//logo_setting
		
		array(
			'id'              => 'sub_header_banner',
			'title'           => esc_html__( 'Sub Header Banner', 'themeum-core' ),
			'description'     => esc_html__( 'sub header banner', 'themeum-core' ),
			'priority'        => 10,
			// 'active_callback' => 'is_front_page',
			'fields'         => array(

				array(
					'settings' => 'sub_header_padding_top',
					'label'    => esc_html__( 'Sub-Header Padding Top', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 90,
				),
				array(
					'settings' => 'sub_header_padding_bottom',
					'label'    => esc_html__( 'Sub-Header Padding Bottom', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 90,
				),
				array(
					'settings' => 'sub_header_margin_bottom',
					'label'    => esc_html__( 'Sub-Header Margin Bottom', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 60,
				),
				array(
					'settings' => 'sub_header_banner_img',
					'label'    => esc_html__( 'Sub-Header Background Image', 'themeum-core' ),
					'type'     => 'upload',
					'priority' => 10,
				),
				array(
					'settings' => 'sub_header_title',
					'label'    => esc_html__( 'Title Settings', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				array(
					'settings' => 'sub_header_title_size',
					'label'    => esc_html__( 'Header Title Font Size', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => '50',
				),
				array(
					'settings' => 'sub_header_title_color',
					'label'    => esc_html__( 'Header Title Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#222538',
				),
			)//fields
		),//sub_header_banner


		array(
			'id'              => 'typo_setting',
			'title'           => esc_html__( 'Typography Setting', 'themeum-core' ),
			'description'     => esc_html__( 'Typography Setting', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(

				array(
					'settings' => 'font_title_body',
					'label'    => esc_html__( 'Body Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//body font
				array(
					'settings' => 'body_google_font',
					'label'    => esc_html__( 'Select Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'body_font_weight',
					'google_font_weight_default' => '400'
				),
				array(
					'settings' => 'body_font_size',
					'label'    => esc_html__( 'Body Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '14',
				),
				array(
					'settings' => 'body_font_height',
					'label'    => esc_html__( 'Body Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '24',
				),
				array(
					'settings' => 'body_font_weight',
					'label'    => esc_html__( 'Body Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '400',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'body_font_color',
					'label'    => esc_html__( 'Body Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#7d91aa',
				),
				array(
					'settings' => 'font_title_menu',
					'label'    => esc_html__( 'Menu Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//Menu font
				array(
					'settings' => 'menu_google_font',
					'label'    => esc_html__( 'Select Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '300'
				),
				array(
					'settings' => 'menu_font_size',
					'label'    => esc_html__( 'Menu Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '16',
				),
				array(
					'settings' => 'menu_font_height',
					'label'    => esc_html__( 'Menu Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '54',
				),
				array(
					'settings' => 'menu_font_weight',
					'label'    => esc_html__( 'Menu Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '300',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'menu_font_color',
					'label'    => esc_html__( 'Menu Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#191919',
				),

				array(
					'settings' => 'font_title_h1',
					'label'    => esc_html__( 'Heading 1 Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//Heading 1
				array(
					'settings' => 'h1_google_font',
					'label'    => esc_html__( 'Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '700'
				),
				array(
					'settings' => 'h1_font_size',
					'label'    => esc_html__( 'Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '42',
				),
				array(
					'settings' => 'h1_font_height',
					'label'    => esc_html__( 'Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '48',
				),
				array(
					'settings' => 'h1_font_weight',
					'label'    => esc_html__( 'Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '700',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'h1_font_color',
					'label'    => esc_html__( 'Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#333',
				),

				array(
					'settings' => 'font_title_h2',
					'label'    => esc_html__( 'Heading 2 Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//Heading 2
				array(
					'settings' => 'h2_google_font',
					'label'    => esc_html__( 'Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '700'
				),
				array(
					'settings' => 'h2_font_size',
					'label'    => esc_html__( 'Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '36',
				),
				array(
					'settings' => 'h2_font_height',
					'label'    => esc_html__( 'Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '36',
				),
				array(
					'settings' => 'h2_font_weight',
					'label'    => esc_html__( 'Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '600',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'h2_font_color',
					'label'    => esc_html__( 'Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#222538',
				),

				array(
					'settings' => 'font_title_h3',
					'label'    => esc_html__( 'Heading 3 Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//Heading 3
				array(
					'settings' => 'h3_google_font',
					'label'    => esc_html__( 'Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '600'
				),
				array(
					'settings' => 'h3_font_size',
					'label'    => esc_html__( 'Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '26',
				),
				array(
					'settings' => 'h3_font_height',
					'label'    => esc_html__( 'Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '28',
				),
				array(
					'settings' => 'h3_font_weight',
					'label'    => esc_html__( 'Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '600',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'h3_font_color',
					'label'    => esc_html__( 'Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#222538',
				),

				array(
					'settings' => 'font_title_h4',
					'label'    => esc_html__( 'Heading 4 Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				//Heading 4
				array(
					'settings' => 'h4_google_font',
					'label'    => esc_html__( 'Heading4 Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '600'
				),
				array(
					'settings' => 'h4_font_size',
					'label'    => esc_html__( 'Heading4 Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '18',
				),
				array(
					'settings' => 'h4_font_height',
					'label'    => esc_html__( 'Heading4 Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '26',
				),
				array(
					'settings' => 'h4_font_weight',
					'label'    => esc_html__( 'Heading4 Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '600',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'h4_font_color',
					'label'    => esc_html__( 'Heading4 Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#222538',
				),

				array(
					'settings' => 'font_title_h5',
					'label'    => esc_html__( 'Heading 5 Font Options', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),

				//Heading 5
				array(
					'settings' => 'h5_google_font',
					'label'    => esc_html__( 'Heading5 Google Font', 'themeum-core' ),
					'type'     => 'select',
					'default'  => 'Roboto',
					'choices'  => get_google_fonts(),
					'google_font' => true,
					'google_font_weight' => 'menu_font_weight',
					'google_font_weight_default' => '600'
				),
				array(
					'settings' => 'h5_font_size',
					'label'    => esc_html__( 'Heading5 Font Size', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '14',
				),
				array(
					'settings' => 'h5_font_height',
					'label'    => esc_html__( 'Heading5 Font Line Height', 'themeum-core' ),
					'type'     => 'number',
					'default'  => '24',
				),
				array(
					'settings' => 'h5_font_weight',
					'label'    => esc_html__( 'Heading5 Font Weight', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '600',
					'choices'  => array(
						'' => esc_html( 'Select', 'themeum-core' ),
						'100' => esc_html( '100', 'themeum-core' ),
						'200' => esc_html( '200', 'themeum-core' ),
						'300' => esc_html( '300', 'themeum-core' ),
						'400' => esc_html( '400', 'themeum-core' ),
						'500' => esc_html( '500', 'themeum-core' ),
						'600' => esc_html( '600', 'themeum-core' ),
						'700' => esc_html( '700', 'themeum-core' ),
						'800' => esc_html( '800', 'themeum-core' ),
						'900' => esc_html( '900', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'h5_font_color',
					'label'    => esc_html__( 'Heading5 Font Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#333',
				),

			)//fields
		),//typo_setting

		array(
			'id'              => 'layout_styling',
			'title'           => esc_html__( 'Layout & Styling', 'themeum-core' ),
			'description'     => esc_html__( 'Layout & Styling', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'boxfull_en',
					'label'    => esc_html__( 'Select BoxWidth of FullWidth', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'fullwidth',
					'choices'  => array(
						'boxwidth' => esc_html__( 'BoxWidth', 'themeum-core' ),
						'fullwidth' => esc_html__( 'FullWidth', 'themeum-core' ),
					)
				),

				array(
					'settings' => 'body_bg_color',
					'label'    => esc_html__( 'Body Background Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#FBFBFB',
				),
				array(
					'settings' => 'body_bg_img',
					'label'    => esc_html__( 'Body Background Image', 'themeum-core' ),
					'type'     => 'image',
					'priority' => 10,
				),
				array(
					'settings' => 'body_bg_attachment',
					'label'    => esc_html__( 'Body Background Attachment', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'fixed',
					'choices'  => array(
						'scroll' => esc_html__( 'Scroll', 'themeum-core' ),
						'fixed' => esc_html__( 'Fixed', 'themeum-core' ),
						'inherit' => esc_html__( 'Inherit', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'body_bg_repeat',
					'label'    => esc_html__( 'Body Background Repeat', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'no-repeat',
					'choices'  => array(
						'repeat' => esc_html__( 'Repeat', 'themeum-core' ),
						'repeat-x' => esc_html__( 'Repeat Horizontally', 'themeum-core' ),
						'repeat-y' => esc_html__( 'Repeat Vertically', 'themeum-core' ),
						'no-repeat' => esc_html__( 'No Repeat', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'body_bg_size',
					'label'    => esc_html__( 'Body Background Size', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'cover',
					'choices'  => array(
						'cover' => esc_html__( 'Cover', 'themeum-core' ),
						'contain' => esc_html__( 'Contain', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'body_bg_position',
					'label'    => esc_html__( 'Body Background Position', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'left top',
					'choices'  => array(
						'left top' => esc_html__('left top', 'themeum-core'),
						'left center' => esc_html__('left center', 'themeum-core'),
						'left bottom' => esc_html__('left bottom', 'themeum-core'),
						'right top' => esc_html__('right top', 'themeum-core'),
						'right center' => esc_html__('right center', 'themeum-core'),
						'right bottom' => esc_html__('right bottom', 'themeum-core'),
						'center top' => esc_html__('center top', 'themeum-core'),
						'center center' => esc_html__('center center', 'themeum-core'),
						'center bottom' => esc_html__('center bottom', 'themeum-core'),
					)
				),
				array(
					'settings' => 'custom_preset_en',
					'label'    => esc_html__( 'Set Custom Color', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'major_color',
					'label'    => esc_html__( 'Major Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),
				array(
					'settings' => 'hover_color',
					'label'    => esc_html__( 'Hover Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#333',
				),
			

				# button color section(new)
				array(
					'settings' => 'button_color_title',
					'label'    => esc_html__( 'Button Color Settings', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				
				array(
					'settings' => 'button_bg_color',
					'label'    => esc_html__( 'Background Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#50a2ff',
				),

				array(
					'settings' => 'button_hover_bg_color',
					'label'    => esc_html__( 'Hover Background Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#1481ff',
				),
				array(
					'settings' => 'button_text_color',
					'label'    => esc_html__( 'Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#fff',
				),
				array(
					'settings' => 'button_hover_text_color',
					'label'    => esc_html__( 'Hover Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#fff',
				),
				array(
					'settings' => 'button_radius',
					'label'    => esc_html__( 'Border Radius', 'themeum-core' ),
					'type'     => 'range',
					'priority' => 10,
					'default'  => '4',
				),
				# end button color section.

				# navbar color section start.
				array(
					'settings' => 'menu_color_title',
					'label'    => esc_html__( 'Menu Color Settings', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				array(
					'settings' => 'navbar_text_color',
					'label'    => esc_html__( 'Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#fff',
				),

				array(
					'settings' => 'navbar_hover_text_color',
					'label'    => esc_html__( 'Hover Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),

				array(
					'settings' => 'navbar_active_text_color',
					'label'    => esc_html__( 'Active Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),

				array(
					'settings' => 'sub_menu_color_title',
					'label'    => esc_html__( 'Sub-Menu Color Settings', 'themeum-core' ),
					'type'     => 'title',
					'priority' => 10,
				),
				array(
					'settings' => 'sub_menu_bg',
					'label'    => esc_html__( 'Background Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#ffffff',
				),
				array(
					'settings' => 'sub_menu_text_color',
					'label'    => esc_html__( 'Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#191919',
				),
				array(
					'settings' => 'sub_menu_text_color_hover',
					'label'    => esc_html__( 'Hover Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),
				#End of the navbar color section


			)//fields
		),//Layout & Styling


		array(
			'id'              => 'social_media_settings',
			'title'           => esc_html__( 'Social Media', 'themeum-core' ),
			'description'     => esc_html__( 'Social Media', 'themeum-core' ),
			'priority'        => 10,
			// 'active_callback' => 'is_front_page',
			'fields'         => array(
				array(
					'settings' => 'wp_facebook',
					'label'    => esc_html__( 'Add Facebook URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '#',
				),
				array(
					'settings' => 'wp_twitter',
					'label'    => esc_html__( 'Add Twitter URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '#',
				),
				array(
					'settings' => 'wp_google_plus',
					'label'    => esc_html__( 'Add Goole Plus URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '#',
				),
				array(
					'settings' => 'wp_pinterest',
					'label'    => esc_html__( 'Add Pinterest URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '#',
				),
				array(
					'settings' => 'wp_youtube',
					'label'    => esc_html__( 'Add Youtube URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_linkedin',
					'label'    => esc_html__( 'Add Linkedin URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_linkedin_user',
					'label'    => esc_html__( 'Linkedin Username( For Share )', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),

				array(
					'settings' => 'wp_instagram',
					'label'    => esc_html__( 'Add Instagram URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '#',
				),
				array(
					'settings' => 'wp_dribbble',
					'label'    => esc_html__( 'Add Dribbble URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_behance',
					'label'    => esc_html__( 'Add Behance URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_flickr',
					'label'    => esc_html__( 'Add Flickr URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_vk',
					'label'    => esc_html__( 'Add Vk URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'wp_skype',
					'label'    => esc_html__( 'Add Skype URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
			)//fields
		),//social_media

		array(
			'id'              => 'coming_soon',
			'title'           => esc_html__( 'Coming Soon', 'themeum-core' ),
			'description'     => esc_html__( 'Coming Soon', 'themeum-core' ),
			'priority'        => 10,
			// 'active_callback' => 'is_front_page',
			'fields'         => array(

				array(
					'settings' => 'comingsoon_en',
					'label'    => esc_html__( 'Enable Coming Soon', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),

				array(
					'settings' => 'comingsoontitle',
					'label'    => esc_html__( 'Add Coming Soon Title', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => esc_html__( 'Coming Soon', 'themeum-core' ),
				),

				array(
					'settings' => 'comingsoon_date',
					'label'    => esc_html__( 'Coming Soon date', 'themeum-core' ),
					'type'     => 'date',
					'priority' => 10,
					'default'  => '2020-08-09',
				),
				array(
					'settings' => 'newsletter',
					'label'    => esc_html__( 'Add mailchimp Form Shortcode Here', 'themeum-core' ),
					'type'     => 'textarea',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'coming_description',
					'label'    => esc_html__( 'Coming Soon Description', 'themeum-core' ),
					'type'     => 'textarea',
					'priority' => 10,
					'default'  => esc_html__('We are come back soon', 'themeum-core'),
				),
				array(
					'settings' => 'comingsoon_twitter',
					'label'    => esc_html__( 'Add Twitter URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_google_plus',
					'label'    => esc_html__( 'Add Google Plus URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_pinterest',
					'label'    => esc_html__( 'Add Pinterest URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_youtube',
					'label'    => esc_html__( 'Add Youtube URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_linkedin',
					'label'    => esc_html__( 'Add Linkedin URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_dribbble',
					'label'    => esc_html__( 'Add Dribbble URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
				array(
					'settings' => 'comingsoon_instagram',
					'label'    => esc_html__( 'Add Instagram URL', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '',
				),
			)//fields
		),//coming_soon
		array(
			'id'              => '404_settings',
			'title'           => esc_html__( '404 Page', 'themeum-core' ),
			'description'     => esc_html__( '404 page background and text settings', 'themeum-core' ),
			'priority'        => 10,
			// 'active_callback' => 'is_front_page',
			'fields'         => array(
				array(
					'settings' => '404_title',
					'label'    => esc_html__( '404 Page Title', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => esc_html__('The page doesn’t exist.', 'themeum-core')
				),
				array(
					'settings' => '404_description',
					'label'    => esc_html__( '404 Page Description', 'themeum-core' ),
					'type'     => 'textarea',
					'priority' => 10,
					'default'  => ''
				),
				array(
					'settings' => '404_btn_text',
					'label'    => esc_html__( '404 Button Text', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => esc_html__('Go Back', 'themeum-core')
				),
			)
		),

		array(
			'id'              => 'blog_setting',
			'title'           => esc_html__( 'Blog Setting', 'themeum-core' ),
			'description'     => esc_html__( 'Blog Setting', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'blog_sidebar',
					'label'    => esc_html__( 'Enable Blog Sidebar', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_sidebar_style',
					'label'    => esc_html__( 'Blog Sidebar Position', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'right',
					'choices'  => array(
						'right' => esc_html( 'Sidebar Right', 'themeum-core' ),
						'left' => esc_html( 'Sidebar Left', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'blog_column',
					'label'    => esc_html__( 'Select Blog Column', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '12',
					'choices'  => array(
						'12' => esc_html( 'Column 1', 'themeum-core' ),
						'6' => esc_html( 'Column 2', 'themeum-core' ),
						'4' => esc_html( 'Column 3', 'themeum-core' ),
						'3' => esc_html( 'Column 4', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'blog_date',
					'label'    => esc_html__( 'Enable Blog Date', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'blog_author',
					'label'    => esc_html__( 'Enable Blog Author', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'blog_category',
					'label'    => esc_html__( 'Enable Blog Category', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_tags',
					'label'    => esc_html__( 'Enable Tags', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),				
				array(
					'settings' => 'blog_hit',
					'label'    => esc_html__( 'Enable Blog Hit Count', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'blog_intro_text_en',
					'label'    => esc_html__( 'Enable Intro Text', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_continue_en',
					'label'    => esc_html__( 'Enable Blog Readmore', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_social_share',
					'label'    => esc_html__( 'Enable Blog Social Share', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'blog_comment',
					'label'    => esc_html__( 'Enable Comment', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => false,
				),
				array(
					'settings' => 'blog_post_text_limit',
					'label'    => esc_html__( 'Post character Limit', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => '220',
				),
				array(
					'settings' => 'blog_continue',
					'label'    => esc_html__( 'Continue Reading', 'themeum-core' ),
					'type'     => 'text',
					'priority' => 10,
					'default'  => 'Read More',
				),
			)//fields
		),//blog_setting



		array(
			'id'              => 'blog_single_setting',
			'title'           => esc_html__( 'Blog Single Page Setting', 'themeum-core' ),
			'description'     => esc_html__( 'Blog Single Page Setting', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'sidebar_single',
					'label'    => esc_html__( 'Blog Single Sidebar', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_single_sidebar',
					'label'    => esc_html__( 'Blog Sidebar Position', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => 'right',
					'choices'  => array(
						'right' => esc_html( 'Sidebar Right', 'themeum-core' ),
						'left' => esc_html( 'Sidebar Left', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'blog_date_single',
					'label'    => esc_html__( 'Enable Blog Date', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_author_single',
					'label'    => esc_html__( 'Enable Blog Author', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_category_single',
					'label'    => esc_html__( 'Enable Blog Category', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_tags_single',
					'label'    => esc_html__( 'Enable Tags', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),				
				array(
					'settings' => 'blog_hit_single',
					'label'    => esc_html__( 'Enable Blog Hit Count', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'blog_comment_single',
					'label'    => esc_html__( 'Enable Comment', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
							
			) #fields
		), 
		#blog_single_page_setting



		array(
			'id'              => 'bottom_setting',
			'title'           => esc_html__( 'Bottom Setting', 'themeum-core' ),
			'description'     => esc_html__( 'Bottom Setting', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'bottom_en',
					'label'    => esc_html__( 'Enable Bottom Area', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'bottom_column',
					'label'    => esc_html__( 'Select Bottom Column', 'themeum-core' ),
					'type'     => 'select',
					'priority' => 10,
					'default'  => '3',
					'choices'  => array(
						'12' => esc_html( 'Column 1', 'themeum-core' ),
						'6' => esc_html( 'Column 2', 'themeum-core' ),
						'4' => esc_html( 'Column 3', 'themeum-core' ),
						'3' => esc_html( 'Column 4', 'themeum-core' ),
					)
				),
				array(
					'settings' => 'bottom_color',
					'label'    => esc_html__( 'Bottom background Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#f5f5f5',
				),
				array(
					'settings' => 'bottom_title_color',
					'label'    => esc_html__( 'Bottom Title Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#000',
				),	
				array(
					'settings' => 'bottom_link_color',
					'label'    => esc_html__( 'Bottom Link Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#808080',
				),				
				array(
					'settings' => 'bottom_hover_color',
					'label'    => esc_html__( 'Bottom link hover color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),
				array(
					'settings' => 'bottom_text_color',
					'label'    => esc_html__( 'Bottom Text color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#808080',
				),
				array(
					'settings' => 'bottom_padding_top',
					'label'    => esc_html__( 'Bottom Top Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 80,
				),	
				array(
					'settings' => 'bottom_padding_bottom',
					'label'    => esc_html__( 'Bottom Padding Bottom', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 45,
				),					
			)//fields
		),//bottom_setting		
		array(
			'id'              => 'footer_setting',
			'title'           => esc_html__( 'Footer Setting', 'themeum-core' ),
			'description'     => esc_html__( 'Footer Setting', 'themeum-core' ),
			'priority'        => 10,
			'fields'         => array(
				array(
					'settings' => 'footer_en',
					'label'    => esc_html__( 'Disable Copyright Area', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'copyright_en',
					'label'    => esc_html__( 'Disable copyright text', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'bottom_footer_menu',
					'label'    => esc_html__( 'Disable Footer Menu', 'themeum-core' ),
					'type'     => 'switch',
					'priority' => 10,
					'default'  => true,
				),
				array(
					'settings' => 'copyright_text',
					'label'    => esc_html__( 'Copyright Text', 'themeum-core' ),
					'type'     => 'textarea',
					'priority' => 10,
					'default'  => esc_html__( '© 2018 Corlate. All Rights Reserved.', 'themeum-core' ),
				),
				array(
					'settings' => 'copyright_text_color',
					'label'    => esc_html__( 'Footer Text Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#fff',
				),				
				array(
					'settings' => 'copyright_link_color',
					'label'    => esc_html__( 'Footer Link Color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#fff',
				),				
				array(
					'settings' => 'copyright_hover_color',
					'label'    => esc_html__( 'Footer link hover color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#EC5538',
				),
				array(
					'settings' => 'copyright_bg_color',
					'label'    => esc_html__( 'Footer background color', 'themeum-core' ),
					'type'     => 'color',
					'priority' => 10,
					'default'  => '#2e2e2e',
				),
				array(
					'settings' => 'copyright_padding_top',
					'label'    => esc_html__( 'Footer Top Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 25,
				),	
				array(
					'settings' => 'copyright_padding_bottom',
					'label'    => esc_html__( 'Footer Bottom Padding', 'themeum-core' ),
					'type'     => 'number',
					'priority' => 10,
					'default'  => 25,
				),					
			)//fields
		),//footer_setting
		
	),
);//themeum-core_panel_options


$framework = new THM_Customize( $panel_to_section );
