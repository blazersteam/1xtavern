<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Corlate_Portfolio{

	public function get_name(){
		return 'corlate_portfolio';
	}
	public function get_title(){
		return 'Corlate Portfolio';
	}
	public function get_icon() {
		return 'wppb-font-Page-grid';
	}
	public function get_category_name(){
        return 'Corlate Addon';
    }

	// headline Settings Fields
	public function get_settings() {

		$settings = array(

			'portfolio_number' => array(
				'type' => 'number',
				'title' => __('Number of Portfolio','floox'),
				'range' => array(
							'min' => 0,
							'max' => 12,
							'step' => 1,
						),
				'std' => '3',
			),
			'portfolio_column' => array(
				'type' => 'select',
				'title' => __('Portfolio Column','floox'),
				'placeholder' => __('Number of Column','floox'),
				'values' => array(
                    '12' =>  __( 'One Column', 'floox' ),
                    '6' =>  __( 'Two Column', 'floox' ),
                    '4' =>  __( 'Three Column', 'floox' ),
                    '3' =>  __( 'Four Column', 'floox' )
                ),
                'std' => '4'
			),
			'portfolio_order_by' => array(
				'type' => 'select',
				'title' => __('Post Order','floox'),
				'placeholder' => __('Order By','floox'),
				'responsive' => true,
				'values' => array(
					'DEC' => 'Descending',
					'ASC' => 'Ascending',
				),
				'std' => array(
					'DEC' => 'Descending',
					'ASC' => 'Ascending',
				),
			),
			'portfolio_show_filter' => array(
				'type' => 'switch',
				'title' => __('Post Filter','floox'),
				'std' => '0'
			),
			'portfolio_show_content' => array(
				'type' => 'switch',
				'title' => __('Show Content','floox'),
				'std' => '0'
			),
			'portfolio_spacing' => array(
				'type' => 'slider',
				'title' => __('Portfolio Spacing','floox'),
				'range' => array(
							'min' => 0,
							'max' => 100,
							'step' => 1,
						),
				'std' => '15px',
				'tab' => 'style',
				'selector' => '{{SELECTOR}} .portfolio-items{ padding: {{data.portfolio_spacing}}; }'
			), 
			'content_bg' => array(
				'type' => 'background2',
				'tab' => 'style',
				'title' => __('Content Background','floox'),
				'selector' => '{{SELECTOR}} .portfolio-item-content',
				'std' => array(
					'bgType' => 'color',
					'bgColor' => '',
					'bgImage' => array(),
					'bgimgPosition' => '',
					'bgimgAttachment' => '',
					'bgimgRepeat' => '',
					'bgimgSize' => '',
					'bgDefaultColor' => '',
					'bgGradient' => array(),
					'Opacity' => 1,
					'bgHoverType' => 'color',
					'bgHoverColor' => '',
					'bgHoverImage' => array(),
					'bgHoverimgPosition' => '',
					'bgHoverimgAttachment' => '',
					'bgimgHoverRepeat' => '',
					'bgimgHoverSize' => '',
					'bgHoverDefaultColor' => '',
					'bgHoverGradient' => array(),
					'hoverOpacity' => 1,
				)
			),
			'filter_alignment' => array(
				'type' => 'alignment',
				'title' => __('Filter Alignment','floox'),
				'std' => 'center',
				'selector' => '{{SELECTOR}} .portfolioFilter{ text-align: {{data.filter_alignment}}; }',
			),
			'filter_title_color'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Filter Title Color',
				'std' => '#575757',
				'selector' => '{{SELECTOR}} .filterable-portfolio .portfolioFilter a{ color: {{data.filter_title_color}}; }'
			),
			'filter_active_color'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Filter Active Color',
				'std' => '#fff',
				'selector' => '{{SELECTOR}} .filterable-portfolio .portfolioFilter a.current{ color: {{data.filter_active_color}}; }'
			),
			'filter_active_bg_color'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Active Background Color',
				'std' => '#ec5538',
				'selector' => '{{SELECTOR}} .filterable-portfolio .portfolioFilter a.current{ background: {{data.filter_active_bg_color}}; }'
			),
			'filter_border'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Filter Border Color',
				'std' => '#F2F2F2',
				'selector' => '{{SELECTOR}} .filterable-portfolio .portfolioFilter a{ border-color: {{data.filter_border}}; }'
			),
			'filter_active_border'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Filter Active Border',
				'std' => '#ec5538',
				'selector' => '{{SELECTOR}} .filterable-portfolio .portfolioFilter a.current{ border-color: {{data.filter_active_border}}; }'
			),
			'content_alignment' => array(
				'type' => 'alignment',
				'title' => __('Content Alignment','floox'),
				'std' => 'center',
				'tab' => 'style', 
				'selector' => '{{SELECTOR}} .portfolio-item-content{ text-align: {{data.content_alignment}}; }',
				'section' => 'Portfolio Content',
				'clip' => true,
			),
			'cont_title_color'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Content Title Color',
				'std' => '#293340',
				'section' => 'Portfolio Content',
				'clip' => true,
				'selector' => '{{SELECTOR}} .portfolio-title a, .overlay-cont2{ color: {{data.cont_title_color}}; }'
			),
			'cont_title_hover'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Content Title Hover',
				'std' => '#293340',
				'section' => 'Portfolio Content',
				'clip' => true,
				'selector' => '{{SELECTOR}} .portfolio-title a:hover, {{SELECTOR}} .overlay-cont2:hover{ color: {{data.cont_title_hover}}; }'
			),
			'sub_title_color'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Sub Title Color',
				'std' => '#6D7784',
				'section' => 'Portfolio Content',
				'clip' => true,
				'selector' => '{{SELECTOR}} .portfolio-category{ color: {{data.sub_title_color}}; }'
			),
			'sub_title_hover'=> array(
				'type' => 'color',
				'tab' => 'style', 
				'title' => 'Sub Title Hover',
				'std' => '#6D7784',
				'section' => 'Portfolio Content',
				'clip' => true,
				'selector' => '{{SELECTOR}} .portfolio-category:hover{ color: {{data.sub_title_hover}}; }'
			),
			'cont_title_fontstyle' => array(
				'type' => 'typography',
				'tab' => 'style', 
				'title' => __('Content Title Typography','floox'),
				'section' => 'Portfolio Content',
				'clip' => true,
				'std' => array(
					'fontFamily' => '',
					'fontSize' => array( 'md'=>'28px', 'sm'=>'', 'xs'=>'' ),
					'lineHeight' => array( 'md'=>'', 'sm'=>'', 'xs'=>'' ),
					'fontWeight' => '700',
					'textTransform' => '',
					'fontStyle' => '',
					'letterSpacing' => array( 'md'=>'', 'sm'=>'', 'xs'=>'' ),
				),
				'selector' => '{{SELECTOR}} .portfolio-title',
				'tab' => 'style',
			),
			'sub_title_fontstyle' => array(
				'type' => 'typography',
				'tab' => 'style', 
				'title' => __('Sub Title Typography','floox'),
				'section' => 'Portfolio Content',
				'clip' => true,
				'std' => array(
					'fontFamily' => '',
					'fontSize' => array( 'md'=>'28px', 'sm'=>'', 'xs'=>'' ),
					'lineHeight' => array( 'md'=>'', 'sm'=>'', 'xs'=>'' ),
					'fontWeight' => '700',
					'textTransform' => '',
					'fontStyle' => '',
					'letterSpacing' => array( 'md'=>'', 'sm'=>'', 'xs'=>'' ),
				),
				'selector' => '{{SELECTOR}} .portfolio-category',
				'tab' => 'style',
			)

		);

		return $settings;
	}

	// Headline Render HTML
	public function render($data = null){

		ob_start();
		$settings 		= $data['settings'];
		$page_numb 			= max( 1, get_query_var('paged') );

		$args = array(
			'post_type'		 	=> 'portfolio',
			'posts_per_page'	=> $settings['portfolio_number'],
			'order'				=> $settings['portfolio_order_by']
		);
		if( $page_numb ){
			$args['paged'] = $page_numb;
		}

		$data = new \WP_Query( $args );
		global $post;
	?>

	<div class="filterable-portfolio <?php echo $settings['portfolio_layout']; ?>">


<?php
// filter of portfolio
if ( $settings['portfolio_show_filter'] == '1' ) {
	$filters = get_terms('portfolio-cat');
	if ( $filters && !is_wp_error( $filters ) ) { ?>
		<div class="portfolioFilter">
			<a class="current" href="#" data-filter="*"><?php esc_html_e('Show All','floox'); ?></a>
		<?php foreach ( $filters as $filter ){ ?>
			<a href="#" data-filter=".<?php echo esc_attr($filter->slug); ?>"><?php echo esc_html($filter->name); ?></a>
		<?php } ?>
		</div>
	<?php }
} ?>


<div class="container-fluid">
	<div class="row portfolioContainer">

	<?php
	if ( $data->have_posts() ) :
		while ( $data->have_posts() ) : $data->the_post();

		$external_link	= esc_attr(get_post_meta( get_the_ID(),'external_link',true));
		# Filter List Item
		$terms	  = get_the_terms(  get_the_ID(), 'portfolio-cat' );
		$term_name  = '';
		if (is_array( $terms )) {
			foreach ( $terms as $term ) {
				$term_name .= ' '.$term->slug;
			}
		}
		# category list
		$terms2 = get_the_terms(  get_the_ID(), 'portfolio-cat' );
		$term_name2 = '';
		if (is_array( $terms2 )){
			foreach ( $terms2 as $term2 )
			{
				$term_name2 .= $term2->slug.', ';
			}
		}
		$term_name2 = substr($term_name2, 0, -2);
		?>

		<div class="portfolio-items col-<?php echo $settings['portfolio_column'];?> <?php echo $term_name; ?>">
			<div class="portfolio-single-items">

				<div class="portfolio-thumb">
					<?php if(has_post_thumbnail( get_the_ID())) {
						$thumb  = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()), 'floox-portfo'); ?>
						<img class="img-responsive" src="<?php echo esc_url($thumb[0]); ?>"  alt="">
					<?php } else { ?>
						<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>'/images/recipes.jpg" alt="<?php _e('image','floox'); ?>">
					<?php } ?>

					<div class="caption-full-width2">
						<div class="overlay-cont">
							<?php if(has_post_thumbnail( get_the_ID() )) {
								$photo = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'full' ); ?>
						<a class="plus-icon cloud-zooms" href="<?php echo esc_url($photo[0]); ?>"><i class="fa fa-plus"></i></a>
					<?php } ?>
						</div>
					</div>

				</div>

				<?php if(!empty($settings['portfolio_show_content'])) { ?>
					<div class="portfolio-item-content">
						<div class="portfolio-item-content-in">
							<i class="floox-icon floox-wave"></i>
							<div>
								<h3 class="portfolio-title">
									<a href="<?php the_permalink(  get_the_ID() ); ?>"><?php echo get_the_title( get_the_ID()) ?></a>
								</h3>

								<?php if($term_name != '') { ?>
									<span class="portfolio-category"><?php echo $term_name2; ?></span>
								<?php } ?>

							</div>
						</div>
					</div>
				<?php } ?>

			</div>
		</div>
	<?php endwhile; ?>
	</div>
</div>

</div>
<?php wp_reset_postdata(); endif; ?>
	<?php
    
        $output = ob_get_contents();
        ob_end_clean(); 
        return $output;

    }
}