<?php
/*
* Plugin Name: Themeum Core
* Plugin URI: http://www.themeum.com/item/core
* Author: Themeum
* Author URI: http://www.themeum.com
* License - GNU/GPL V2 or Later
* Description: Themeum Core is a required plugin for this theme.
* Version: 1.0.0
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// language
add_action( 'init', 'themeum_core_language_load' );
function themeum_core_language_load(){
    $plugin_dir = basename(dirname(__FILE__))."/languages/";
    load_plugin_textdomain( 'themeum-core', false, $plugin_dir );
}

# portfoio register
require_once('post-type/portfolio.php');

//widget
require_once('widgets/blog-posts.php');
require_once('widgets/image_widget.php');
require_once('widgets/themeum_about_widget.php');
require_once('widgets/themeum_social_share.php');


// Custom Customizer
include_once( 'customizer/libs/googlefonts.php' );
include_once( 'customizer/customizer.php' );

/* ------------------------------------------
*               Customizer
* ------------------------------------------- */
function corlate_customize_control_js() {
    wp_enqueue_script( 'color-preset-control', corlate_JS . 'color-scheme-control.js', array( 'customize-controls', 'iris', 'underscore', 'wp-util' ), '20141216', true );
}
add_action( 'customize_controls_enqueue_scripts', 'corlate_customize_control_js' );


# Add CSS for Frontend
add_action( 'wp_enqueue_scripts', 'themeum_core_style' );
if(!function_exists('themeum_core_style')):
    function themeum_core_style(){
        # CSS
        wp_enqueue_style('themeum-core-css',plugins_url('assets/css/themeum-core.css',__FILE__));
        wp_enqueue_style('reduxadmin-css',plugins_url('assets/css/themeum-core.css',__FILE__));

        # JS
        wp_enqueue_script('custom',plugins_url('assets/js/custom.js',__FILE__), array('jquery'));
        wp_enqueue_script('isotop',plugins_url('assets/js/jquery.isotope.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('magnific',plugins_url('assets/js/jquery.magnific-popup.min.js',__FILE__), array('jquery'));
        
    }
endif;

function themeum_load_admin_assets() {
    wp_enqueue_script( 'themeum-admin', plugins_url('assets/js/admin.js', __FILE__), array('jquery') );
}
add_action( 'admin_enqueue_scripts', 'themeum_load_admin_assets' );


// Metabox Include
include_once( 'meta_box.php' );
include_once( 'meta-box/meta-box.php' );


//WPPB Hook
add_filter( 'wppb_available_addons', 'prefix_custom_addon_include' );
if ( ! function_exists('prefix_custom_addon_include')){
    function prefix_custom_addon_include($addons){
        $addons[] = 'Corlate_Portfolio';
        // Add Other Custom Addon class name in here, at a time.
        return $addons;
    }
}

//Addon List Item
require 'wppb-addon/wppb-portfolio.php';
